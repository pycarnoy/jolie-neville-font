# Jolie Neville

Font experimentation — an hommage with formal constraints to 80s and 90s fonts such as Modula by Zuzana Licko, Industria by Neville Brody, FF Rosetta by Max Kisman...

Just a few hours work on Glyphs3. It will be updated soon... or not!

![Jolie Neville nano specimen](images/jns-nano-specimen-001.png)
